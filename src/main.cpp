#include <Arduino.h>

typedef struct
{
  uint8_t pin;
  bool value;
  unsigned long timestamp;
} button_t;

const uint8_t NUM_BUTTONS = 8;
button_t buttons[NUM_BUTTONS] = {
  { GPIO_NUM_16 },
  { GPIO_NUM_17 },
  { GPIO_NUM_18 },
  { GPIO_NUM_19 },
  { GPIO_NUM_21 },
  { GPIO_NUM_22 },
  { GPIO_NUM_23 },
  { GPIO_NUM_25 }
};

const uint32_t DEBOUNCE_TIME   = 50; // ms
const uint32_t LONG_PRESS_TIME = 400; //ms

int i;
bool value;
unsigned long timestamp, press_time;


void short_press(uint8_t pin) {
  switch (pin)
  {
    case GPIO_NUM_16:
      Serial.println("Arrow up");
      break;
    case GPIO_NUM_17:
      Serial.println("Arrow right");
      break;
    case GPIO_NUM_18:
      Serial.println("Arrow down");
      break;
    case GPIO_NUM_19:
      Serial.println("Arrow left");
      break;

    default:
      Serial.println("No action");
      break;
  }
}

void long_press(uint8_t pin) {
  switch (pin)
  {
    case GPIO_NUM_16:
      Serial.println("Volume up");
      break;
    case GPIO_NUM_17:
      Serial.println("Next track");
      break;
    case GPIO_NUM_18:
      Serial.println("Volume down");
      break;
    case GPIO_NUM_19:
      Serial.println("Previous track");
      break;

    default:
      Serial.println("No action");
      break;
  }
}

void check_button(button_t *button)
{
  // use inverted value because pin is pulled to ground
  value = !digitalRead(button->pin);
  timestamp = millis();

  // time difference
  press_time = timestamp - button->timestamp;

  // debounce
  if (press_time < DEBOUNCE_TIME) return;

  // check button state
  if (value != button->value) {

    // set button state and timestamp
    button->value = value;
    button->timestamp = timestamp;

    // on button press
    if (value) { return; }

    // on button release
    // check short or long press
    bool is_short_press = press_time < LONG_PRESS_TIME;

    Serial.printf("GPIO%d: [%lu ms] %s | ", button->pin, press_time, is_short_press ? "short" : "long");

    if (is_short_press) {
      short_press(button->pin);
    } else {
      long_press(button->pin);
    }
  }
}

void check_button_task(void* arg)
{
  // forever loop
  for (;;) {
    // loop over buttons
    for (i = 0; i < NUM_BUTTONS; i++) check_button(&buttons[i]);

    // task delay
    vTaskDelay(1 / portTICK_RATE_MS);
  }
}

void initialize_gpio() {
  for (int i = 0; i < NUM_BUTTONS; i++) pinMode(buttons[i].pin, INPUT_PULLUP);
}

void setup() {
  // Initialize gpio pins
  initialize_gpio();

  // Initialize serial port
  Serial.begin(115200);

  // Start gpio task
  xTaskCreate(check_button_task, "check_button_task", 1024, NULL, 10, NULL);
}

void loop() { }
